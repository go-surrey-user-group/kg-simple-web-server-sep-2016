# Simple Web Server

This application provides a simple web application to demonstrate CRUD
(Create, Read, Update & Delete) operations on an in-memory non-persistant
store.

The webserver listens on all network interfaces on port 8080.

URL on local machine: http://localhost:8080/

We make use of the third party Gorilla mux library to provide regex matching
in urls & method routing.

Due to the small amount of HTML required, I have chosen to keep the raw HTML
in this go file as string constants, making the entire application just one
executable to deploy. The HTML renders without CSS or if an Internet connection
is available then it uses Bootstrap to make it pretty.

Author: Kevin Golding
