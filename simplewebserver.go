/*
	This application provides a simple web application to demonstrate CRUD
	(Create, Read, Update & Delete) operations on an in-memory non-persistant
	store.

	The webserver listens on all network interfaces on port 8080.

	URL on local machine: http://localhost:8080/

	We make use of the third party Gorilla mux library to provide regex matching
	in urls & method routing.

	Due to the small amount of HTML required, I have chosen to keep the raw HTML
	in this go file as string constants, making the entire application just one
	executable to deploy. The HTML renders without CSS or if an Internet connection
	is available then it uses Bootstrap to make it pretty.
*/
package main

import (
	"errors"
	"fmt"
	"html/template"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

// A Person
type PersonStruct struct {
	// The json tags are not currently used, but are here for future use
	Id       int    `json:"id,required"`
	Forename string `json:"forename,required"`
	Surname  string `json:"surname,required"`
	Email    string `json:"email"`
}

// In-memory map of Person's using the persons Id as the map key
var PersonDatabase map[int]PersonStruct

// Global var holding the templates
var templates *template.Template

func init() {
	// Initiase the in-memory database
	PersonDatabase = make(map[int]PersonStruct)

	// Add demo data
	PersonDatabase[1] = PersonStruct{1, "Kevin", "Golding", "kevin@kgolding.co.uk"}
	PersonDatabase[2] = PersonStruct{2, "Harry", "Howdoyoudo", "harry@domain.com"}

	// Create templates
	templates = template.Must(template.New("Page").Parse(HtmlPage))
	template.Must(templates.New("PersonList").Parse(HtmlPersonList))
	template.Must(templates.New("PersonDetail").Parse(HtmlPersonDetail))
	template.Must(templates.New("PersonForm").Parse(HtmlPersonForm))
}

func main() {
	// mux is a http compliant router from the Gorilla set of libs
	r := mux.NewRouter()

	// Redirect to / to /person making it the default page
	r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, "/person", http.StatusTemporaryRedirect)
	})

	// Person routes
	r.HandleFunc("/person", PersonList).Methods("GET")
	r.HandleFunc("/person/{id:[0-9]+}", PersonGet).Methods("GET")
	r.HandleFunc("/person/edit/{id:[0-9]+}", PersonEdit).Methods("GET")
	r.HandleFunc("/person/create", PersonCreate).Methods("Get")
	r.HandleFunc("/person/{id:[0-9]+}", PersonPost).Methods("POST")
	r.HandleFunc("/person/{id:[0-9]+}", PersonDelete).Methods("DELETE")

	// Start the server listening on all interfaces on port 8080
	http.ListenAndServe("0.0.0.0:8080", r)
}

// http handler to return a list of all Persons
func PersonList(w http.ResponseWriter, r *http.Request) {
	err := templates.Lookup("PersonList").Execute(w, PersonDatabase)
	if err != nil {
		StringResponse(w, err.Error(), http.StatusInternalServerError)
	}
}

// http handler to display a single Person
func PersonGet(w http.ResponseWriter, r *http.Request) {
	id, err := GetVarsInt(r, "id")
	if err != nil {
		StringResponse(w, "Invalid id", http.StatusBadRequest)
		return
	}
	person, ok := PersonDatabase[id]
	if ok {
		err := templates.Lookup("PersonDetail").Execute(w, person)
		if err != nil {
			StringResponse(w, err.Error(), http.StatusInternalServerError)
		}
		return
	}
	w.WriteHeader(http.StatusNoContent)
}

// http handler to display a Person in an editable form
func PersonEdit(w http.ResponseWriter, r *http.Request) {
	id, err := GetVarsInt(r, "id")
	if err != nil {
		StringResponse(w, "Invalid id", http.StatusOK)
	}
	person, ok := PersonDatabase[id]
	if ok {
		err := templates.Lookup("PersonForm").Execute(w, person)
		if err != nil {
			StringResponse(w, err.Error(), http.StatusInternalServerError)
		}
		return
	}
	w.WriteHeader(http.StatusNoContent)
}

// http handler to display a new/blank Person form
func PersonCreate(w http.ResponseWriter, r *http.Request) {
	err := templates.Lookup("PersonForm").Execute(w, &PersonStruct{})
	if err != nil {
		StringResponse(w, err.Error(), http.StatusInternalServerError)
	}
}

// http handler to save a posted Person and redirect to a detail view
func PersonPost(w http.ResponseWriter, r *http.Request) {
	// Get id from url
	id, err := GetVarsInt(r, "id")
	if err != nil {
		StringResponse(w, "Invalid id", http.StatusOK)
	}

	person, err := PersonFromHttpRequest(r)
	if err != nil {
		StringResponse(w, err.Error(), http.StatusNotAcceptable)
		return
	}

	if id == 0 { // Create new
		// Get highest current id and add 1 to it for the new id
		newId := 1
		for k, _ := range PersonDatabase {
			if k >= id {
				newId = k + 1
			}
		}
		person.Id = newId
		PersonDatabase[newId] = person
	} else { // Update existing person based on id, else add new using given id
		person.Id = id
		PersonDatabase[id] = person
	}

	// Display detail view using page refresh to change the url and prevent the
	// "F5" button re-post issue
	w.Header().Add("Refresh", fmt.Sprintf("0, url=/person/%d", person.Id))
	w.WriteHeader(http.StatusOK)
}

// http handler to delete a Person and redirect to the list view
func PersonDelete(w http.ResponseWriter, r *http.Request) {
	// Get id from url
	id, err := GetVarsInt(r, "id")
	if err != nil {
		StringResponse(w, "Invalid id", http.StatusOK)
		return
	}

	delete(PersonDatabase, id)

	w.WriteHeader(http.StatusOK)
}

// Create a Person from the posted data in the given *http.Request. Returns errors
// if there are missing fields or a non integer Id
func PersonFromHttpRequest(r *http.Request) (PersonStruct, error) {
	p := PersonStruct{}

	r.ParseForm()

	sid := r.Form.Get("id")
	id64, err := strconv.ParseInt(sid, 10, 64)
	if err != nil {
		//		return p, errors.New("Invalid Id field")
	}
	p.Id = int(id64)

	p.Forename = r.Form.Get("forename")
	if p.Forename == "" {
		return p, errors.New("Missing Forename field")
	}

	p.Surname = r.Form.Get("surname")
	if p.Forename == "" {
		return p, errors.New("Missing Surname field")
	}

	p.Email = r.Form.Get("email")
	if p.Email == "" {
		//		return p, errors.New("Missing Email field")
	}

	return p, nil
}

/******************************** Helpers ********************************/

// Returns a Int for a given *http.Requests/mux.Vars
func GetVarsInt(r *http.Request, key string) (int, error) {

	params := mux.Vars(r)
	if s, ok := params[key]; ok {
		i64, err := strconv.ParseInt(s, 10, 64)
		if err != nil {
			return 0, err
		}
		return int(i64), nil
	}
	return 0, errors.New("Not found")
}

// Writes a String and Status code to a ResponseWriter
func StringResponse(w http.ResponseWriter, str string, code int) {
	w.Write([]byte(str))
	w.WriteHeader(code)
}

/**************************** HTML Templates ****************************/

// The main HTML body split up into Header and Footer templates
const HtmlPage = `
{{ define "Header" }}
<html>
<head>
	<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script>
		function DeletePerson(id) {
			if (confirm("Delete record?") === true) {
				var xmlhttp = new XMLHttpRequest();
				xmlhttp.onreadystatechange = function() {
				        if (xmlhttp.readyState == XMLHttpRequest.DONE ) {
				           if (xmlhttp.status == 200) {
				               window.location = "/person"
				           } else {
				               alert("Error deleting record: " + xmlhttp.responseText);
				           }
				        }
				    };
				xmlhttp.open("DELETE", "/person/" + id, true);
	    		xmlhttp.send();
			}
		}
	</script>
</head>
<body>
	<div class="container">
		<h1><a href="http://www.meetup.com/Surrey-Go-User-Group-Meetup/"
			target="_blank">Surrey Go User Group</a>
			- Simple Web Server</h1>
		<hr>
		<p>
			<a href="/person"
				class="btn btn-info btn-lg">List view</a> &nbsp;
			<a href="/person/create"
				class="btn btn-default btn-lg pull-right">Create new</a>
		</p>
		<hr>
	{{ end }}
	{{ define "Footer" }}
	</div>
</body>
</html>
{{ end }}
`

// List of Person's, which calls in the Header & Footer templates from above
const HtmlPersonList = `
{{ template "Header" }}
	<h2>Person List</h2>
	<table cellpadding=3 class="table table-striped table-hover">
	<thead>
		<tr>
			<th>Id</th>
			<th>Forename</th>
			<th>Surname</th>
			<th>Email</th>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		{{ range . }}
		<tr>
			<td>	<a href="/person/{{ .Id }}" class="badge">
					{{ .Id }}</a></td>
			<td>{{ .Forename }}</td>
			<td>{{ .Surname }}</td>
			<td>{{ .Email }}</td>
			<td class="text-right"><a href="/person/{{ .Id }}" class="btn btn-primary">
				View</a>
				&nbsp;
				<a href="/person/edit/{{ .Id }}" class="btn btn-warning">
				Edit</a>
				&nbsp;
				<button onclick="DeletePerson({{ .Id }})"
					class="btn btn-danger">Delete</button>

			</td>
		</tr>
		{{ end }}
	</tbody>
	</table>
{{ template "Footer" }}
`

// Person detail view, which calls in the Header & Footer templates from above
const HtmlPersonDetail = `
{{ template "Header" }}
	<h2>Person Details</h2>
	<table cellpadding=3 class="table">
	<thead>
		<tr>
			<th>Field</th>
			<th>Value</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Id:</td>
			<td>{{ .Id }}</td>
		</tr>
		<tr>
			<td>Forename:</td>
			<td>{{ .Forename }}</td>
		</tr>
		<tr>
			<td>Surname:</td>
			<td>{{ .Surname }}</td>
		</tr>
		<tr>
			<td>Email:</td>
			<td>{{ .Email }}</td>
		</tr>
		<tr>
			<td></td>
			<td><a href="/person/edit/{{ .Id }}" class="btn btn-warning">
				Edit</a>
				&nbsp;
				<button onclick="DeletePerson({{ .Id }})"
					class="btn btn-danger">Delete</button>
			</td>
		</tr>
	</tbody>
	</table>
{{ template "Footer"}}
`

// Person detail form, which calls in the Header & Footer templates from above
const HtmlPersonForm = `
{{ template "Header" }}
	<h2>{{ if .Id }}Edit{{ else }}Add{{ end }} Person</h2>
	<form action="/person/{{ .Id }}" method="post">
		<table cellpadding=3 class="table">
		<thead>
			<tr>
				<th>Field</th>
				<th>Value</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Id:</td>
				<td>{{ if .Id }}{{ .Id }}{{ else }}n/a{{ end }}</td>
			</tr>
			<tr>
				<td>Forename:</td>
				<td><input type="text" value="{{ .Forename }}" name="forename"
						class="form-control"></td>
			</tr>
			<tr>
				<td>Surname:</td>
				<td><input type="text" value="{{ .Surname }}" name="surname"
						class="form-control"></td>
			</tr>
			<tr>
				<td>Email:</td>
				<td><input type="email" value="{{ .Email }}" name="email"
						class="form-control"></td>
			</tr>
			<tr>
				<td></td>				
				<td><input type="submit" value="Save"
					class="btn btn-primary"></td>	
			</tr>
		</tbody>
		</table>
	</form>
{{ template "Footer"}}
`
